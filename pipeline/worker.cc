#include "common.hpp"
#include "options.hpp"
#include <zmq.hpp>
#include <unistd.h>		// getpid()
#include <sstream>
#include <iomanip>
#include <stdlib.h>		// atoi

void worker(int portin, int portout, int id, int n_tasks, int n_workers);

int main(int argc, char* argv[])
{
	if (opts.verbose == 2)
		printf("worker:%d: argc=%d\n", getpid(), argc);
	if (argc != 6)
		return 1;
	if (opts.verbose == 2)
		printf("worker:%d: %s %s %s %s\n", getpid(), argv[1], argv[2], argv[3], argv[4]);
	worker(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]));
}

void connect(zmq::socket_t& in, zmq::socket_t& out, int port_in, int port_out);
void do_work(zmq::socket_t& in, zmq::socket_t& out, int id, int n_tasks, int n_workers);

void worker(int port_in, int port_out, int id, int n_tasks, int n_workers)
{
	zmq::context_t ctx(1);
	zmq::socket_t in(ctx, zmq::socket_type::pull);
	zmq::socket_t out(ctx, zmq::socket_type::push);

	connect(in, out, port_in, port_out);
	do_work(in, out, id, n_tasks, n_workers);
}

void connect(zmq::socket_t& in, zmq::socket_t& out, int port_in, int port_out)
{
	string in_addr = fmt("tcp://localhost:%d", port_in);
	string out_addr = fmt("tcp://localhost:%d", port_out);
	if (opts.verbose == 1)
		printf("worker:%d: in=%s out=%s\n", getpid(), in_addr.c_str(), out_addr.c_str());

	in.connect(in_addr);
	out.connect(out_addr);
}

void do_work(zmq::socket_t& in, zmq::socket_t& out, int id, int n_tasks, int n_workers)
{
	{ zmq::message_t msg; in.recv(&msg); }

	real_t sum;
	{
		real_t range = real_t(id)/real_t(n_workers);
		real_t dx = real_t(1)/(real_t(n_tasks)*real_t(n_workers));
		for (int i = 0; i < n_tasks; ++i)
		{
			real_t x = range + i*dx;
			real_t area = rect(x, dx, f);

			sum += area;
		}
	}

	std::ostringstream os;
	{
		using namespace std;
		os << setprecision(28) << setw(35) << sum;
	}
	string str = os.str();

	zmq::message_t msg(str.size());
	memcpy(msg.data(), str.c_str(), msg.size());
	out.send(msg);

	printf("id=%d rect=%s\n", id, str.c_str());
}
