#pragma once
#include <string>
#include <stdio.h>

typedef std::string string;

int spawn(const char* prog, ...);
string fmt(const char* fmt, ...) __attribute__((format(printf, 1, 2)));

// work to be done:
//	Find the Riemann Sum of: y = x*x where x:[0, 1)
//	We use high precission floating point math
//	Each worker has n tasks, n workers
#include <mpfr_real/real.hpp>

typedef mpfr::real<4096> real_t;

inline real_t f(real_t x)
{
	return x*x;
}

inline
real_t rect(real_t x, real_t dx, real_t (*f)(real_t))
{
	real_t lh = abs(f(x));
	return lh*dx;
}
