#include "common.hpp"
#include "options.hpp"
#include <unistd.h>		// sleep
#include <sys/types.h>	// pid_t, wait types
#include <sys/wait.h>	// waitpid
#include <zmq.hpp>
#include <vector>
#include <iomanip>
#include <stdlib.h>		// atoi

void pipeline(int port_in, int port_out, int n_tasks, int n_workers);

int main(int argc, char* argv[])
{
	if (opts.verbose == 2)
		printf("pipeline:%d: argc=%d\n", getpid(), argc);
	if (argc != 5)
		return 1;
	pipeline(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
}

typedef std::vector<pid_t> pids_t;

void bind(zmq::socket_t& source, zmq::socket_t& sink, int in, int out);
pids_t spawn_workers(int portin, int portout, int n_tasks, int n_workers);
void wait_workers(pids_t& pids);
void send_work(zmq::socket_t& source, int n_workers);
void collect(zmq::socket_t& sink, int n_workers);

void pipeline(int port_in, int port_out, int n_tasks, int n_workers)
{
	// establish source/sink points for workers
	zmq::context_t ctx(1);
	zmq::socket_t in(ctx, zmq::socket_type::push);
	zmq::socket_t out(ctx, zmq::socket_type::pull);
	bind(in, out, port_in, port_out);

	pids_t pids = spawn_workers(port_in, port_out, n_tasks, n_workers);
	send_work(in, n_workers);
	collect(out, n_workers);
	wait_workers(pids);
}

void bind(zmq::socket_t& source, zmq::socket_t& sink, int in, int out)
{
	string in_addr = fmt("tcp://*:%d", in);
	string out_addr = fmt("tcp://*:%d", out);
	if (opts.verbose == 1)
		printf("pipeline:%d: in=%s out=%s\n", getpid(), in_addr.c_str(), out_addr.c_str());

	// establish source/sink points for workers
	source.bind(in_addr);
	sink.bind(out_addr);
}

pids_t spawn_workers(int port_in, int port_out, int n_tasks, int n_workers)
{
	// fire up local workers
	string port_in_str = fmt("%d", port_in);
	string port_out_str = fmt("%d", port_out);
	string tasks = fmt("%d", n_tasks);
	string workers = fmt("%d", n_workers);
	pids_t pids(n_workers);
	for (int i = 0; i < n_workers; ++i)
	{
		string id = fmt("%d", i);
		pids[i] = spawn(
			"./worker",
			port_in_str.c_str(), port_out_str.c_str(),
			id.c_str(), tasks.c_str(), workers.c_str(),
			nullptr);
		if (opts.verbose == 2)
			printf("pipeline:%d: spawn ./worker id=%d pid=%d\n", getpid(), i, pids[i]);
	}

	return pids;
}

void wait_workers(pids_t& pids)
{
	// wait for local workers to shut down
	int state;
	for (size_t i = 0; i != pids.size(); ++i)
	{
		waitpid(pids[i], &state, WEXITED);
		if (opts.verbose == 1)
			printf("pipeline:%d: spawn ./worker id=%zu pid=%d ret=%d\n", getpid(), i, pids[i], WEXITSTATUS(state));
	}
}

void send_work(zmq::socket_t& source, int n_workers)
{
	sleep(1);
	zmq::message_t msg(5);
	for (int i = 0; i < n_workers; ++i)
		source.send(msg);
}

void collect(zmq::socket_t& sink, int n_workers)
{
	real_t sum(0), val;
	for (int i = 0; i < n_workers; ++i)
	{
		zmq::message_t msg;
		sink.recv(&msg);

		std::istringstream is(string((char*)msg.data(), msg.size()));
		is >> val;
		sum += val;
	}

	using namespace std;
	cout << "area=" << setprecision(28) << setw(35) << sum << endl;
}
